# Ordered Assignment

This repository contains two versions of a Java program to solve an assignment problem with a side constraint requiring monotonicity. The problem is specified to have $m$ sources and $n$ sinks, with $m \lt n$. The monotonicity constraint says that if source $i$ is assigned to sink $j$, then source $i+1$ can only be assigned to one of the sinks $j+1,\dots,n$.

The repository contains two branches, "master" and "CPLEX". Both require the open-source [algs4 library](https://algs4.cs.princeton.edu/code/). The "CPLEX" branch contains a MIP model that is used both to confirm the validity of the network model and to compare solution speed. It requires that a recent version of [CPLEX Studio](https://www.ibm.com/products/ilog-cplex-optimization-studio) be installed. The "master" branch does not contain the MIP model and requires only the algs4 library.

The problem originated in a [post](https://stackoverflow.com/questions/65884107/minimize-sum-of-product-of-two-uneven-consecutive-arrays) on Stack Overflow. Erwin Kalvelagen proposed a MIP model as a solution and [posted some results](https://yetanothermathprogrammingconsultant.blogspot.com/2021/01/an-assignment-problem-with-wrinkle.html) related to it on his blog. In the comments section of the blog post, several people proposed network models like the one demonstrated by this code.

An explanation of the network model can be found in a post on [my blog](https://orinanobworld.blogspot.com/2021/01/a-monotonic-assignment-problem.html).