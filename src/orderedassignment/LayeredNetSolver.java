package orderedassignment;

import edu.princeton.cs.algs4.AcyclicSP;
import edu.princeton.cs.algs4.DirectedEdge;
import edu.princeton.cs.algs4.EdgeWeightedDigraph;
import java.util.ArrayList;

/**
 * LayeredNetSolver sets up a layered digraph for the assignment problem and
 * solves it.
 *
 * Assume sources are indexed 0,...,m-1 and sinks are indexed 0,...,n-1 with
 * m less than n. The digraph is constructed as follows.
 *
 * Layer 0: start node
 * Layer i (i = 1, ..., m):  nodes j = i-1 ... n-m+i-1 representing assignment
 * of source i-1 to sink j
 * Layer m+1: finish node
 *
 * There is an arc from the start node to each node in layer 1 whose weight
 * is the cost of assigning source 0 to that sink. For each subsequent layer i
 * up to i = m-1, there is an arc from each node j to each node j' greater than
 * j in the next layer, with weight equal to the cost of assigning source i-1
 * to sink j'. From each node in layer m, there is a zero-weight arc to the
 * finish node in layer m+1.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class LayeredNetSolver {
  private final EdgeWeightedDigraph graph;  // the layered digraph
  private final AcyclicSP sp;               // the shortest path solution
  private final int nSources;               // number of sources
  private final int nSinks;                 // number of sinks
  private final double[] a;                 // source "costs"
  private final double[] b;                 // sink "costs"
  private final ArrayList<Node> nodes;      // associates each digraph node
                                            // index with the corresponding node
  private final ArrayList<ArrayList<Node>> layers;
                                            // the list of nodes in each layer
                                            // (excluding dummy nodes)
  private final Node startNode;             // start node
  private final Node finishNode;            // finish node

  /**
   * Constructor.
   * @param a0 the source costs
   * @param b0 the sink costs
   */
  public LayeredNetSolver(final double[] a0, final double[] b0) {
    a = a0;
    b = b0;
    nSources = a.length;
    nSinks = b.length;
    // Create a Node instance for each node in the digraph and add it to
    // the master Node list and the correct layer list.
    int ix = 0;  // running node index
    nodes = new ArrayList<>();
    layers = new ArrayList<>();
    startNode = new Node(ix, -1, -1);
    nodes.add(startNode);
    ix += 1;
    for (int i = 0; i < nSources; i++) {
      ArrayList<Node> layer = new ArrayList<>();
      for (int j = i; j <= nSinks - nSources + i; j++) {
        Node n = new Node(ix, i, j);
        ix += 1;
        nodes.add(n);
        layer.add(n);
      }
      layers.add(layer);
    }
    finishNode = new Node(ix, -1, -1);
    nodes.add(finishNode);
    // Create the digraph instance.
    graph = new EdgeWeightedDigraph(finishNode.index + 1);
    // Add the arcs from the start node to all nodes in the first layer.
    int i = startNode.index;
    long arcCount = 0;
    double c0 = a[0];
    for (Node n : layers.get(0)) {
      graph.addEdge(new DirectedEdge(i, n.index, c0 * b[n.sink]));
      arcCount += 1;
    }
    // Add arcs between consecutive layers.
    for (int ell = 0; ell < layers.size() - 1; ell++) {
      c0 = a[ell + 1];
      for (Node n1 : layers.get(ell)) {
        for (Node n2 : layers.get(ell + 1)) {
          // Add arcs only when sink index at head exceeds sink index at tail.
          if (n2.sink > n1.sink) {
            graph.addEdge(new DirectedEdge(n1.index, n2.index,
                                           c0 * b[n2.sink]));
            arcCount += 1;
          }
        }
      }
    }
    // Add arcs from the final layer to the finish node.
    for (Node n1 : layers.get(layers.size() - 1)) {
      graph.addEdge(new DirectedEdge(n1.index, finishNode.index, 0));
      arcCount += 1;
    }
    // Report the graph size.
    System.out.println("The digraph has " + nodes.size() + " nodes and "
                       + arcCount + " arcs.");
    // Set up (and solve during construction) the acyclic shortest path object.
    sp = new AcyclicSP(graph, 0);
  }

  /**
   * Extracts the optimal assignments.
   * @return a vector containing the sink to which each source is assigned.
   */
  public int[] getAssignments() {
    int[] sol = new int[nSources];
    // Convert the shortest path to an assignment vector.
    for (DirectedEdge e : sp.pathTo(finishNode.index)) {
      // Get the head of the next arc.
      Node n = nodes.get(e.to());
      // Add it to the assignment unless it is the finish node.
      if (n.sink >= 0) {
        sol[n.source] = n.sink;
      }
    }
    return sol;
  }

  /**
   * Gets the cost of the optimal assignment.
   * @return the optimal cost
   */
  public double getCost() {
    return sp.distTo(finishNode.index);
  }

  /**
   * Node represents a node in the digraph.
   * It combines the index of the node in the EdgeWeightedDigraph (where nodes
   * are indexed 0, 1, ...) with the index of the original source and sink.
   * Source and sink indices are -1 at some dummy nodes (where no source or
   * sink is being represented).
   */
  private class Node {
    private int index;   // node number in the edge weighted digraph
    private int source;  // source being assigned (determined by layer)
    private int sink;    // sink being assigned

    /**
     * Constructor.
     * @param ix the node index in the digraph
     * @param ia the index of the source (-1 if a dummy node)
     * @param ib the index of the sink (-1 if a dummy node)
     */
    Node(final int ix, final int ia, final int ib) {
      index = ix;
      source = ia;
      sink = ib;
    }
  }
}
