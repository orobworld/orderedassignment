package orderedassignment;

import edu.princeton.cs.algs4.Stopwatch;
import java.util.Random;

/**
 * OrderedAssignment tries to solve an assignment problem with a monotonicity
 * side constraint as described in
 * https://yetanothermathprogrammingconsultant.blogspot.com/2021/01/
 * an-assignment-problem-with-wrinkle.html as a shortest path problem over
 * a layered digraph.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class OrderedAssignment {

  /**
   * Dummy constructor.
   */
  private OrderedAssignment() { }

  /**
   * Generates a problem and solves it.
   * @param args the command line arguments (unused)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Set the problem dimensions as in Erwin's post.
    int m = 100;   // sources
    int n = 1000;  // sinks
    // Generate random problem data using U(0, 100) distributions (as in
    // Erwin's post).
    Random rng = new Random(12721);
    double[] a = rng.doubles(m, 0, 100).toArray();
    double[] b = rng.doubles(n, 0, 100).toArray();
    // Create the layered network solver (which automatically computes shortest
    // paths) and time it.
    Stopwatch watch = new Stopwatch();
    LayeredNetSolver solver = new LayeredNetSolver(a, b);
    double elapsedTime = watch.elapsedTime();
    System.out.println("Constructing and solving the network took "
                       + elapsedTime + " seconds.");
    System.out.println("The optimal assignment costs " + solver.getCost()
                       + ".\n");
    // Display the solution and directly compute its cost as a check.
    int[] solution = solver.getAssignments();
    double cost = 0;
    for (int i = 0; i < m; i++) {
      int j = solution[i];
      double z = a[i] * b[j];
      cost += z;
      System.out.println(i + " -> " + j + " (cost " + cost + ")");
    }
    System.out.println("\nDirectly computed cost = " + cost + ".");
  }

}
